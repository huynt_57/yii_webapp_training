<?php

/**
 * This is the model class for table "hoadon".
 *
 * The followings are the available columns in table 'hoadon':
 * @property integer $MaHD
 * @property integer $MaKH
 * @property integer $MaTT
 * @property string $TenKH
 * @property string $DiaChi
 * @property string $NgayMua
 * @property string $NgayGiao
 *
 * The followings are the available model relations:
 * @property Khachhang $maKH
 * @property Phuongthuctt $maTT
 * @property Sanpham[] $sanphams
 */
class Hoadon extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'hoadon';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('MaKH, MaTT', 'numerical', 'integerOnly'=>true),
			array('TenKH', 'length', 'max'=>25),
			array('DiaChi', 'length', 'max'=>50),
			array('NgayMua, NgayGiao', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('MaHD, MaKH, MaTT, TenKH, DiaChi, NgayMua, NgayGiao', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'maKH' => array(self::BELONGS_TO, 'Khachhang', 'MaKH'),
			'maTT' => array(self::BELONGS_TO, 'Phuongthuctt', 'MaTT'),
			'sanphams' => array(self::MANY_MANY, 'Sanpham', 'hoadonchitiet(MaHD, MaSP)'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'MaHD' => 'Ma Hd',
			'MaKH' => 'Ma Kh',
			'MaTT' => 'Ma Tt',
			'TenKH' => 'Ten Kh',
			'DiaChi' => 'Dia Chi',
			'NgayMua' => 'Ngay Mua',
			'NgayGiao' => 'Ngay Giao',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('MaHD',$this->MaHD);
		$criteria->compare('MaKH',$this->MaKH);
		$criteria->compare('MaTT',$this->MaTT);
		$criteria->compare('TenKH',$this->TenKH,true);
		$criteria->compare('DiaChi',$this->DiaChi,true);
		$criteria->compare('NgayMua',$this->NgayMua,true);
		$criteria->compare('NgayGiao',$this->NgayGiao,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Hoadon the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
