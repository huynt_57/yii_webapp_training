<?php

/**
 * This is the model class for table "khachhang".
 *
 * The followings are the available columns in table 'khachhang':
 * @property integer $MaKH
 * @property string $TenKH
 * @property string $DiaChi
 * @property string $Email
 * @property string $SoDT
 * @property string $TaiKhoan
 * @property string $MatKhau
 * @property integer $TrangThai
 *
 * The followings are the available model relations:
 * @property Binhluan[] $binhluans
 * @property Hoadon[] $hoadons
 */
class Khachhang extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'khachhang';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('TrangThai', 'numerical', 'integerOnly'=>true),
			array('TenKH, TaiKhoan', 'length', 'max'=>25),
			array('DiaChi', 'length', 'max'=>100),
			array('Email', 'length', 'max'=>50),
			array('SoDT', 'length', 'max'=>12),
			array('MatKhau', 'length', 'max'=>32),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('MaKH, TenKH, DiaChi, Email, SoDT, TaiKhoan, MatKhau, TrangThai', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'binhluans' => array(self::HAS_MANY, 'Binhluan', 'MaKH'),
			'hoadons' => array(self::HAS_MANY, 'Hoadon', 'MaKH'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'MaKH' => 'Ma Kh',
			'TenKH' => 'Ten Kh',
			'DiaChi' => 'Dia Chi',
			'Email' => 'Email',
			'SoDT' => 'So Dt',
			'TaiKhoan' => 'Tai Khoan',
			'MatKhau' => 'Mat Khau',
			'TrangThai' => 'Trang Thai',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('MaKH',$this->MaKH);
		$criteria->compare('TenKH',$this->TenKH,true);
		$criteria->compare('DiaChi',$this->DiaChi,true);
		$criteria->compare('Email',$this->Email,true);
		$criteria->compare('SoDT',$this->SoDT,true);
		$criteria->compare('TaiKhoan',$this->TaiKhoan,true);
		$criteria->compare('MatKhau',$this->MatKhau,true);
		$criteria->compare('TrangThai',$this->TrangThai);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Khachhang the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
