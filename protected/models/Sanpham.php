<?php

/**
 * This is the model class for table "sanpham".
 *
 * The followings are the available columns in table 'sanpham':
 * @property integer $MaSP
 * @property integer $MaLoaiSP
 * @property integer $MaHangSX
 * @property string $TenSP
 * @property double $GiaSP
 * @property integer $TrangThai
 * @property string $HinhAnh
 * @property string $MoTaSP
 *
 * The followings are the available model relations:
 * @property Binhluan[] $binhluans
 * @property Hoadon[] $hoadons
 * @property Hangsx $maHangSX
 * @property Loaisp $maLoaiSP
 */
class Sanpham extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'sanpham';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('MaLoaiSP, MaHangSX, TrangThai', 'numerical', 'integerOnly'=>true),
			array('GiaSP', 'numerical'),
			array('TenSP', 'length', 'max'=>200),
			array('HinhAnh', 'length', 'max'=>50),
			array('MoTaSP', 'length', 'max'=>10000),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('MaSP, MaLoaiSP, MaHangSX, TenSP, GiaSP, TrangThai, HinhAnh, MoTaSP', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'binhluans' => array(self::HAS_MANY, 'Binhluan', 'MaSP'),
			'hoadons' => array(self::MANY_MANY, 'Hoadon', 'hoadonchitiet(MaSP, MaHD)'),
			'maHangSX' => array(self::BELONGS_TO, 'Hangsx', 'MaHangSX'),
			'maLoaiSP' => array(self::BELONGS_TO, 'Loaisp', 'MaLoaiSP'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'MaSP' => 'Ma Sp',
			'MaLoaiSP' => 'Ma Loai Sp',
			'MaHangSX' => 'Ma Hang Sx',
			'TenSP' => 'Ten Sp',
			'GiaSP' => 'Gia Sp',
			'TrangThai' => 'Trang Thai',
			'HinhAnh' => 'Hinh Anh',
			'MoTaSP' => 'Mo Ta Sp',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('MaSP',$this->MaSP);
		$criteria->compare('MaLoaiSP',$this->MaLoaiSP);
		$criteria->compare('MaHangSX',$this->MaHangSX);
		$criteria->compare('TenSP',$this->TenSP,true);
		$criteria->compare('GiaSP',$this->GiaSP);
		$criteria->compare('TrangThai',$this->TrangThai);
		$criteria->compare('HinhAnh',$this->HinhAnh,true);
		$criteria->compare('MoTaSP',$this->MoTaSP,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Sanpham the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
