<?php

class SiteController extends Controller
{
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
                $Criteria = new CDbCriteria();//represent for query such as conditions, ordering by, limit/offset. 
                $Criteria -> select = "*";
                $Criteria -> order = "TenHangSX ASC";
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
		
                
                
                $newCriteria = new CDbCriteria();
                $newCriteria -> select =  "*";
                $newCriteria->limit = "8";
                
                $this->render('index', array ('newPrd' => Sanpham::model()->findAll($newCriteria), 'Companies' => Company::model()->findAll($Criteria)));
	}

	/**
	 * This is the action to handle external exceptions.
	 */
        
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}
        
        public function actionDetails()
	{
                
                
                $comCriteria1 = new CDbCriteria();
                $comCriteria1->select = "`MaHangSX`, `TenHangSX`";
                
                
        
             if ($_GET["pid"])
                {
                    $spCriteria = new CDbCriteria();
                    $spCriteria->select = "*";
                    $spCriteria->condition = "MaSP = ".$_GET["pid"];
                    
                    
                    $this->render ('details',array( 'prdd' => Sanpham::model()->findAll($spCriteria), 'com1'=>  Hangsx::model()->findAll($comCriteria1)));
                    
                }
             else{
            
		$this->mes = "Hello from store/details";
                $this->render('indexs', array('content'=>$this->mes));}
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$name='=?UTF-8?B?'.base64_encode($model->name).'?=';
				$subject='=?UTF-8?B?'.base64_encode($model->subject).'?=';
				$headers="From: $name <{$model->email}>\r\n".
					"Reply-To: {$model->email}\r\n".
					"MIME-Version: 1.0\r\n".
					"Content-Type: text/plain; charset=UTF-8";

				mail(Yii::app()->params['adminEmail'],$subject,$model->body,$headers);
				Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model));
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
				$this->redirect(Yii::app()->user->returnUrl=array('/admin/sanpham/index'));
                            //    $this->redirect(Yii::app()->user->returnUrlarray->('/admin/sanpham/index'));
		}
		// display the login form
		$this->render('login',array('model'=>$model));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
}