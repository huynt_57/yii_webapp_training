<?php

class StoreController extends Controller
{
    public $mes;
    public function actionIndex()
	{
                $this->mes = "Hello from store";
		$this->render('index', array('content'=>$this->mes));
	}
        
        public function actionBrowse()
	{
             
            if ($_GET["id"])
            {
                $comCriteria = new CDbCriteria();
                $comCriteria->select = "`MaHangSX`, `TenHangSX`";
                $comCriteria->condition = "MaHangSX = ".$_GET["id"];
                
                $comCriteria1 = new CDbCriteria();
                $comCriteria1->select = "`MaHangSX`, `TenHangSX`";
               
                
                
                $kprdCriteria = new CDbCriteria();
                $kprdCriteria -> alias = "t1";
                $kprdCriteria->select = "DISTINCT `t1`.`TenLoaiSP`, `t1`.`MaLoaiSP`";
                $kprdCriteria->join = "INNER JOIN `sanpham` ON `sanpham`.`MaLoaiSP` = `t1`.`MaLoaiSP` ";
                $kprdCriteria->condition = "`sanpham`.`MaHangSX`= ".$_GET["id"];
                
                
                $prdCriteria = new CDbCriteria();
          
                $prdCriteria->select = "*";
                $prdCriteria->condition = "MaHangSX = ".$_GET["id"];
                
                
                $count= Sanpham::model()->count($prdCriteria);
                $pages=new CPagination($count);
                
                $pages->pageSize=5;
                $pages->applyLimit($prdCriteria);
               
    
                $this->render ('index',array( 'prd' => Sanpham::model()->findAll($prdCriteria),
                                               'kprd' => Loaisp::model()->findAll($kprdCriteria),
                                                'com'=>  Hangsx::model()->findAll($comCriteria),
                                                 'com1'=>  Hangsx::model()->findAll($comCriteria1)
                                                ,'pages' => $pages));
            } 
            else
            {
               
                
                $this->mes = "Hello from store/Browse";
		$this->render('index', array('content'=>$this->mes));
            }
	}
        
        public function actionDetails()
	{
                
                
                $comCriteria1 = new CDbCriteria();
                $comCriteria1->select = "`MaHangSX`, `TenHangSX`";
                
                
        
             if ($_GET["pid"])
                {
                    $spCriteria = new CDbCriteria();
                    $spCriteria->select = "*";
                    $spCriteria->condition = "MaSP = ".$_GET["pid"];
                    
                    
                    $this->render ('details',array( 'prdd' => Sanpham::model()->findAll($spCriteria), 'com1'=>  Hangsx::model()->findAll($comCriteria1)));
                    
                }
             else{
            
		$this->mes = "Hello from store/details";
                $this->render('indexs', array('content'=>$this->mes));}
	}
        
        public function actionStarRatingAjax() {

       $ratingAjax=isset($_POST['rate']) ? $_POST['rate'] : 0;

       echo "You are voting $ratingAjax through AJAX!";

   }


	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}