<?php
/* @var $this SanphamController */
/* @var $model Sanpham */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'sanpham-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'MaLoaiSP'); ?>
		<?php echo $form->textField($model,'MaLoaiSP'); ?>
		<?php echo $form->error($model,'MaLoaiSP'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'MaHangSX'); ?>
		<?php echo $form->textField($model,'MaHangSX'); ?>
		<?php echo $form->error($model,'MaHangSX'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'TenSP'); ?>
		<?php echo $form->textField($model,'TenSP',array('size'=>60,'maxlength'=>200)); ?>
		<?php echo $form->error($model,'TenSP'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'GiaSP'); ?>
		<?php echo $form->textField($model,'GiaSP'); ?>
		<?php echo $form->error($model,'GiaSP'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'TrangThai'); ?>
		<?php echo $form->textField($model,'TrangThai'); ?>
		<?php echo $form->error($model,'TrangThai'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'HinhAnh'); ?>
		<?php echo $form->textField($model,'HinhAnh',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'HinhAnh'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'MoTaSP'); ?>
		<?php echo $form->textField($model,'MoTaSP',array('size'=>60,'maxlength'=>10000)); ?>
		<?php echo $form->error($model,'MoTaSP'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->