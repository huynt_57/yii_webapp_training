<?php
/* @var $this SanphamController */
/* @var $model Sanpham */

$this->breadcrumbs=array(
	'Sanphams'=>array('index'),
	$model->MaSP,
);

$this->menu=array(
	array('label'=>'List Sanpham', 'url'=>array('index')),
	array('label'=>'Create Sanpham', 'url'=>array('create')),
	array('label'=>'Update Sanpham', 'url'=>array('update', 'id'=>$model->MaSP)),
	array('label'=>'Delete Sanpham', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->MaSP),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Sanpham', 'url'=>array('admin')),
);
?>

<h1>View Sanpham #<?php echo $model->MaSP; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'MaSP',
		'MaLoaiSP',
		'MaHangSX',
		'TenSP',
		'GiaSP',
		'TrangThai',
		'HinhAnh',
		'MoTaSP',
	),
)); ?>
