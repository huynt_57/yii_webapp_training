<?php
/* @var $this SanphamController */
/* @var $data Sanpham */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('MaSP')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->MaSP), array('view', 'id'=>$data->MaSP)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('MaLoaiSP')); ?>:</b>
	<?php echo CHtml::encode($data->MaLoaiSP); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('MaHangSX')); ?>:</b>
	<?php echo CHtml::encode($data->MaHangSX); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TenSP')); ?>:</b>
	<?php echo CHtml::encode($data->TenSP); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('GiaSP')); ?>:</b>
	<?php echo CHtml::encode($data->GiaSP); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TrangThai')); ?>:</b>
	<?php echo CHtml::encode($data->TrangThai); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('HinhAnh')); ?>:</b>
	<?php echo CHtml::encode($data->HinhAnh); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('MoTaSP')); ?>:</b>
	<?php echo CHtml::encode($data->MoTaSP); ?>
	<br />

	*/ ?>

</div>