<?php
/* @var $this StoreController */

$this->breadcrumbs = array(
    'Store',
);
?>
<h1><?php echo $this->id . '/' . $this->action->id; ?></h1>

<div id="menul">
    <?php foreach ($com1 as $Company): ?> 
        <ul>
            <li><?php echo CHtml::link($Company->TenHangSX, array('store/browse', 'id' => $Company->MaHangSX)); ?></li>
        </ul>
    <?php endforeach; ?>
    <img src="http://localhost/Yii_WebApp_Training/images/lenovo.jpg"/>
</div>

<div id="slide">
    <?php
    $this->widget('ext.slider.slider', array(
        'container' => 'slideshow',
        'width' => 725,
        'height' => 300,
        'timeout' => 4000,
        'infos' => false,
        'imagesPath' => 'images',
        'constrainImage' => true,
        'images' => array('01.jpg', '02.jpg', '03.jpg', '04.jpg', '05.jpg', '06.jpg', '07.jpg'),
        //'alts'=>array('First description','Second description','Third description','Four description'),
        'defaultUrl' => Yii::app()->request->hostInfo
            )
    );
    ?>
</div>
<img src="http://localhost/Yii_WebApp_Training/images/bar1.png" width="710" />
<div class="wrapper" >

    <div>
        <div class="ListProduct">

            <div class="listProductContent" style="width: 700px">
                <table cellSanphamacing="0" cellpadding="0" border="0" width="100%">
                    <tr>

                        <?php foreach ($prd as $Sanpham): ?>
                      
                        <td class="img" style="width:180px;">
                            <div id="<?php echo $Sanpham->MaSP ?>" class="tooltip listProductContentImg" style="position:relative;">
                                <img src="http://localhost/Yii_WebApp_Training/images/<?php echo $Sanpham->HinhAnh ?>" width="160" >
                            </div>
                        </td>
                        <td class="vtop" style="width:260px;">
                            <div class="listProductContentDesc">
                                <p class="title"><?php echo CHtml::link($Sanpham->TenSP, array('store/details', 'pid' => $Sanpham->MaSP)); ?></a></p>
                                <p class="desc" >
                                </p>
                            </div>
                        </td>
                        <td class="vtop" style="padding-left:20px; ">
                            <div class="listProductContentOrder">
                                <p class="price" style="font-size:16px;"> <?php echo $Sanpham->GiaSP ?> triệu VNĐ</p>



                                <p class="warranty"><strong>Bảo hành:</strong> 12 Tháng chính hãng tại Việt Nam</p>
                                <p class="selloff"><strong>Khuyến mại:</strong> Tặng túi chống sốc, tặng dán kê tay, tặng dán màn hình bảo vệ, giảm 10% giá phụ kiện khi mua kèm máy</p>	


                                <p id=item_<?php echo $Sanpham->MaSP ?> ></p>
                            </div>
                        </td>

                        </tr> <?php endforeach; ?>	
                    <?php
                    $this->widget('CLinkPager', array(
                        'pages' => $pages,
                    ))
                    ?>
                </table>


               



                  
                            
                            