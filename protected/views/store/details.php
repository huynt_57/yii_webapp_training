<?php
/* @var $this StoreController */

$this->breadcrumbs = array(
    'Store',
);
?>
<?php Yii::app()->clientScript->registerCoreScript('jquery'); ?>

<div id="fb-root"></div>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id))
            return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/vi_VN/all.js#xfbml=1";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
<!-- AddThis Smart Layers BEGIN -->
<!-- Go to http://www.addthis.com/get/smart-layers to customize -->
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=xa-529a08826ae2d2d9"></script>

<script type="text/javascript">
    addthis.layers({
        'theme': 'transparent',
        'share': {
            'position': 'left',
            'numPreferredServices': 5
        },
        'follow': {
            'services': [
                {'service': 'facebook', 'id': 'huy'},
                {'service': 'twitter', 'id': 'huy'}
            ]
        },
        'whatsnext': {},
        'recommended': {}
    });
</script>
<!-- AddThis Smart Layers END -->
<div id="slide">
    <?php
    $this->widget('ext.slider.slider', array(
        'container' => 'slideshow',
        'width' => 905,
        'height' => 300,
        'timeout' => 4000,
        'infos' => false,
        'imagesPath' => 'images',
        'constrainImage' => true,
        'images' => array('01.jpg', '02.jpg', '03.jpg', '04.jpg', '05.jpg', '06.jpg', '07.jpg'),
        //'alts'=>array('First description','Second description','Third description','Four description'),
        'defaultUrl' => Yii::app()->request->hostInfo
            )
    );
    ?>
</div>
<?php foreach ($prdd as $Sanpham): ?>       
    <div class="productDetailTitle"></div>
    <div>
        <div class="fb-like" data-layout="standard" data-href = "http://localhost/Yii_WebApp_Training/index.php/store/details"data-action="like" data-show-faces="true" data-share="true"></div>
        <div class="productDetailCol1">
            <table class="tb_product_detail">
                <tr>
                    <td  id="productImageBox" style="border:0px solid #CCCCCC;padding:5px;">
                        <div style="position:relative;width:260px;">

                            <div style="position:absolute;right:0;top:0;color:#FFF;width:45px;height:35px;line-height:35px;text-align:center;background:url('includes/images/icon_hot.gif') no-repeat left center;">&nbsp;</div>                        <a href="media/product/1125_3422_260.jpg" class="thickbox" rel="pro-gallery2"><img src="http://localhost/Yii_WebApp_Training/images/<?php echo $Sanpham->HinhAnh ?>" alt="<?php echo $Sanpham->TenSP ?>" height="260" width="260"/></a>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div id="proImageThum">
                        </div>
                    </td>
                </tr>
            </table>



        </div>



        <div class="productDetailCol2">
            <p style="color:#0b53a2;font-size: 18px;margin-bottom: 3px;"><strong><?php echo $Sanpham->TenSP ?></strong></p>
            <p style="color:#808285;">Ma san pham: <?php echo $Sanpham->MaSP ?> </p>
            <p>.....................................................................................................................</p>
            <p><?php echo $Sanpham->MoTaSP ?></p>
            <br />

            <p style="height:22px; line-height:22px;vertical-align:middle;">
                <img src= "http://localhost/Yii_WebApp_Training/images/price_icon.png" align="absmiddle" width="16" height="16"> 
                <strong style="font-size: 18px;">Giá bán:</strong> 
            <Sanphaman style="color:#CC0000;font-size: 18px;"><strong><?php echo $Sanpham->GiaSP ?></strong></Sanphaman>
            <Sanphaman style="color:#000;font-size: 14px;"><strong></strong></Sanphaman>
            </p>		

            <p style="height:22px; line-height:22px;vertical-align:middle;"><img src= "http://localhost/Yii_WebApp_Training/images/conhang_icon.jpg" align="absmiddle" width="16" height="16"> <strong>Tình trạng:</strong> <Sanphaman style="font-weight:bold;color:#1B44AC;">Còn hàng</Sanphaman></p>
            <p style="height:22px; line-height:22px;vertical-align:middle;"><img src= "http://localhost/Yii_WebApp_Training/images/wrranty_icon.png" align="absmiddle" width="16" height="16"> <strong>Bảo hành:</strong> 12 months</p>
            <p style="vertical-align:middle;"><img src= "http://localhost/Yii_WebApp_Training/images/sellOffer_icon.png" align="absmiddle" width="16" height="16"> <strong>Khuyến mại:</strong> Giảm 10% khi mua phụ kiện kèm máy</p>
            <p style="height:22px; line-height:22px;vertical-align:middle;"><img src= "http://localhost/Yii_WebApp_Training/images/support_icon.gif" align="absmiddle" width="16" height="16"> <strong>Hỗ trợ: 01679263615</strong> </p>		
            <p style="height:22px; line-height:22px;vertical-align:middle;"><img src= "http://localhost/Yii_WebApp_Training/images/phu_kien_icon.png" align="absmiddle" width="16" height="16"> <strong>Phụ kiện:</strong> </p>
            <p style="margin-top:10px;">
                <?php
                $this->widget('CStarRating', array(
                    'name' => 'ratingAjax',
                    'callback' => '

                function(){

                $.ajax({

                type: "POST",

                url: "' . Yii::app()->createUrl('UiModule/ui_other/starRatingAjax') . '",

                data: "' . Yii::app()->request->csrfTokenName . '=' . Yii::app()->request->getCsrfToken() . '&rate=" + $(this).val(),

                success: function(msg){

                $("#result").html(msg);

                }})}'
                ));

                echo "<br/>";

               
                ?>

                <a href="#" 	 onClick="chon_muahang(<?php echo $Sanpham->MaSP ?>, '<?php echo $Sanpham->TenSP ?>', <?php echo $Sanpham->GiaSP ?>)"><img src="http://localhost/Yii_WebApp_Training/images/cartBG1.png"  /></a>

            </p>


            <p id=item_<?php echo $Sanpham->MaSP ?> ></p>
        </div>
        <div id="socialNetwork" style="padding-left:60px;padding-top:10px;">

            <p>
            <div class="fb-comments" data-href = "http://localhost/Yii_WebApp_Training/index.php/store/details"data-width="700" data-numposts="5" data-colorscheme="light"></div>
<?php endforeach;
?>
