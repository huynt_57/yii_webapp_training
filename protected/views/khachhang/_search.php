<?php
/* @var $this KhachhangController */
/* @var $model Khachhang */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'MaKH'); ?>
		<?php echo $form->textField($model,'MaKH'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TenKH'); ?>
		<?php echo $form->textField($model,'TenKH',array('size'=>25,'maxlength'=>25)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'DiaChi'); ?>
		<?php echo $form->textField($model,'DiaChi',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Email'); ?>
		<?php echo $form->textField($model,'Email',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'SoDT'); ?>
		<?php echo $form->textField($model,'SoDT',array('size'=>12,'maxlength'=>12)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TaiKhoan'); ?>
		<?php echo $form->textField($model,'TaiKhoan',array('size'=>25,'maxlength'=>25)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'MatKhau'); ?>
		<?php echo $form->textField($model,'MatKhau',array('size'=>32,'maxlength'=>32)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TrangThai'); ?>
		<?php echo $form->textField($model,'TrangThai'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->