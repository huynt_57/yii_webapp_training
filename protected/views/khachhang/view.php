<?php
/* @var $this KhachhangController */
/* @var $model Khachhang */

$this->breadcrumbs=array(
	'Khachhangs'=>array('index'),
	$model->MaKH,
);

$this->menu=array(
	array('label'=>'List Khachhang', 'url'=>array('index')),
	array('label'=>'Create Khachhang', 'url'=>array('create')),
	array('label'=>'Update Khachhang', 'url'=>array('update', 'id'=>$model->MaKH)),
	array('label'=>'Delete Khachhang', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->MaKH),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Khachhang', 'url'=>array('admin')),
);
?>

<h1>View Khachhang #<?php echo $model->MaKH; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'MaKH',
		'TenKH',
		'DiaChi',
		'Email',
		'SoDT',
		'TaiKhoan',
		'MatKhau',
		'TrangThai',
	),
)); ?>
