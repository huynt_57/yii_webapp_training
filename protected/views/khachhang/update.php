<?php
/* @var $this KhachhangController */
/* @var $model Khachhang */

$this->breadcrumbs=array(
	'Khachhangs'=>array('index'),
	$model->MaKH=>array('view','id'=>$model->MaKH),
	'Update',
);

$this->menu=array(
	array('label'=>'List Khachhang', 'url'=>array('index')),
	array('label'=>'Create Khachhang', 'url'=>array('create')),
	array('label'=>'View Khachhang', 'url'=>array('view', 'id'=>$model->MaKH)),
	array('label'=>'Manage Khachhang', 'url'=>array('admin')),
);
?>

<h1>Update Khachhang <?php echo $model->MaKH; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>