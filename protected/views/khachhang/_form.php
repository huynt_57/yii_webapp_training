<?php
/* @var $this KhachhangController */
/* @var $model Khachhang */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'khachhang-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'TenKH'); ?>
		<?php echo $form->textField($model,'TenKH',array('size'=>25,'maxlength'=>25)); ?>
		<?php echo $form->error($model,'TenKH'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'DiaChi'); ?>
		<?php echo $form->textField($model,'DiaChi',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'DiaChi'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Email'); ?>
		<?php echo $form->textField($model,'Email',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'Email'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'SoDT'); ?>
		<?php echo $form->textField($model,'SoDT',array('size'=>12,'maxlength'=>12)); ?>
		<?php echo $form->error($model,'SoDT'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'TaiKhoan'); ?>
		<?php echo $form->textField($model,'TaiKhoan',array('size'=>25,'maxlength'=>25)); ?>
		<?php echo $form->error($model,'TaiKhoan'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'MatKhau'); ?>
		<?php echo $form->textField($model,'MatKhau',array('size'=>32,'maxlength'=>32)); ?>
		<?php echo $form->error($model,'MatKhau'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'TrangThai'); ?>
		<?php echo $form->textField($model,'TrangThai'); ?>
		<?php echo $form->error($model,'TrangThai'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->