<?php
/* @var $this KhachhangController */
/* @var $data Khachhang */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('MaKH')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->MaKH), array('view', 'id'=>$data->MaKH)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TenKH')); ?>:</b>
	<?php echo CHtml::encode($data->TenKH); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('DiaChi')); ?>:</b>
	<?php echo CHtml::encode($data->DiaChi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Email')); ?>:</b>
	<?php echo CHtml::encode($data->Email); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('SoDT')); ?>:</b>
	<?php echo CHtml::encode($data->SoDT); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TaiKhoan')); ?>:</b>
	<?php echo CHtml::encode($data->TaiKhoan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('MatKhau')); ?>:</b>
	<?php echo CHtml::encode($data->MatKhau); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('TrangThai')); ?>:</b>
	<?php echo CHtml::encode($data->TrangThai); ?>
	<br />

	*/ ?>

</div>