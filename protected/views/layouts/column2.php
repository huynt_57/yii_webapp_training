<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>
<div class="span-19">
	<div id="content">
		<?php echo $content; ?>
	</div><!-- content -->
</div>
<div class="span-5 last">
	<div id="sidebar">
	<?php
        
        
		$this->beginWidget('zii.widgets.CPortlet', array(
			'title'=>'Operations',
		));
                
                 $this->widget('zii.widgets.CMenu', array(
			'items'=>$this->menu,
			
			'htmlOptions'=>array('class'=>'operations'),
		));$this->endWidget();
                $this->beginWidget('zii.widgets.CPortlet', array(
			'title'=>'Router',
		));
		$this->widget('zii.widgets.CMenu', array(
			'items'=>$this->menu,
			'items'=>array('class'=>'operations'),
                        'items'=>array(
				array('label'=>'Quan ly san pham', 'url'=>array('/sanpham/index')),
				array('label'=>'Quan ly hang sx', 'url'=>array('hangsx/index')),
				array('label'=>'Quan ly khach hang', 'url'=>array('/khachhang/index')),
                                //array('label'=>'admin', 'url'=>array('/admin/sanpham/index')),
				array('label'=>'Login', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
				array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest),
                               
                            //'htmlOptions'=>array('class'=>'operations'),
			),
			'htmlOptions'=>array('class'=>'operations'),
		));
                
               
		$this->endWidget();
	?>
	</div><!-- sidebar -->
</div>
<?php $this->endContent(); ?>