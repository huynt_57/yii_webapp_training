<?php
/* @var $this HoadonController */
/* @var $model Hoadon */

$this->breadcrumbs=array(
	'Hoadons'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Hoadon', 'url'=>array('index')),
	array('label'=>'Manage Hoadon', 'url'=>array('admin')),
);
?>

<h1>Create Hoadon</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>