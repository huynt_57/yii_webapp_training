<?php
/* @var $this HoadonController */
/* @var $data Hoadon */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('MaHD')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->MaHD), array('view', 'id'=>$data->MaHD)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('MaKH')); ?>:</b>
	<?php echo CHtml::encode($data->MaKH); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('MaTT')); ?>:</b>
	<?php echo CHtml::encode($data->MaTT); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TenKH')); ?>:</b>
	<?php echo CHtml::encode($data->TenKH); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('DiaChi')); ?>:</b>
	<?php echo CHtml::encode($data->DiaChi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('NgayMua')); ?>:</b>
	<?php echo CHtml::encode($data->NgayMua); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('NgayGiao')); ?>:</b>
	<?php echo CHtml::encode($data->NgayGiao); ?>
	<br />


</div>