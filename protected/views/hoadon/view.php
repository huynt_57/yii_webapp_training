<?php
/* @var $this HoadonController */
/* @var $model Hoadon */

$this->breadcrumbs=array(
	'Hoadons'=>array('index'),
	$model->MaHD,
);

$this->menu=array(
	array('label'=>'List Hoadon', 'url'=>array('index')),
	array('label'=>'Create Hoadon', 'url'=>array('create')),
	array('label'=>'Update Hoadon', 'url'=>array('update', 'id'=>$model->MaHD)),
	array('label'=>'Delete Hoadon', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->MaHD),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Hoadon', 'url'=>array('admin')),
);
?>

<h1>View Hoadon #<?php echo $model->MaHD; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'MaHD',
		'MaKH',
		'MaTT',
		'TenKH',
		'DiaChi',
		'NgayMua',
		'NgayGiao',
	),
)); ?>
