<?php
/* @var $this HoadonController */
/* @var $model Hoadon */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'hoadon-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'MaKH'); ?>
		<?php echo $form->textField($model,'MaKH'); ?>
		<?php echo $form->error($model,'MaKH'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'MaTT'); ?>
		<?php echo $form->textField($model,'MaTT'); ?>
		<?php echo $form->error($model,'MaTT'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'TenKH'); ?>
		<?php echo $form->textField($model,'TenKH',array('size'=>25,'maxlength'=>25)); ?>
		<?php echo $form->error($model,'TenKH'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'DiaChi'); ?>
		<?php echo $form->textField($model,'DiaChi',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'DiaChi'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'NgayMua'); ?>
		<?php echo $form->textField($model,'NgayMua'); ?>
		<?php echo $form->error($model,'NgayMua'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'NgayGiao'); ?>
		<?php echo $form->textField($model,'NgayGiao'); ?>
		<?php echo $form->error($model,'NgayGiao'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->