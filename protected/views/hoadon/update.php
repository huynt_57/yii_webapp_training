<?php
/* @var $this HoadonController */
/* @var $model Hoadon */

$this->breadcrumbs=array(
	'Hoadons'=>array('index'),
	$model->MaHD=>array('view','id'=>$model->MaHD),
	'Update',
);

$this->menu=array(
	array('label'=>'List Hoadon', 'url'=>array('index')),
	array('label'=>'Create Hoadon', 'url'=>array('create')),
	array('label'=>'View Hoadon', 'url'=>array('view', 'id'=>$model->MaHD)),
	array('label'=>'Manage Hoadon', 'url'=>array('admin')),
);
?>

<h1>Update Hoadon <?php echo $model->MaHD; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>