<?php
/* @var $this HoadonController */
/* @var $model Hoadon */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'MaHD'); ?>
		<?php echo $form->textField($model,'MaHD'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'MaKH'); ?>
		<?php echo $form->textField($model,'MaKH'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'MaTT'); ?>
		<?php echo $form->textField($model,'MaTT'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TenKH'); ?>
		<?php echo $form->textField($model,'TenKH',array('size'=>25,'maxlength'=>25)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'DiaChi'); ?>
		<?php echo $form->textField($model,'DiaChi',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'NgayMua'); ?>
		<?php echo $form->textField($model,'NgayMua'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'NgayGiao'); ?>
		<?php echo $form->textField($model,'NgayGiao'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->