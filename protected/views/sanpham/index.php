<?php
/* @var $this SanphamController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Sanphams',
);

$this->menu=array(
	array('label'=>'Create Sanpham', 'url'=>array('create')),
	array('label'=>'Manage Sanpham', 'url'=>array('admin')),
);
?>

<h1>Sanphams</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
