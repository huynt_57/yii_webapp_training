<?php
/* @var $this SanphamController */
/* @var $model Sanpham */

$this->breadcrumbs=array(
	'Sanphams'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Sanpham', 'url'=>array('index')),
	array('label'=>'Manage Sanpham', 'url'=>array('admin')),
);
?>

<h1>Create Sanpham</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>