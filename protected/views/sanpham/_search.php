<?php
/* @var $this SanphamController */
/* @var $model Sanpham */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'MaSP'); ?>
		<?php echo $form->textField($model,'MaSP'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'MaLoaiSP'); ?>
		<?php echo $form->textField($model,'MaLoaiSP'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'MaHangSX'); ?>
		<?php echo $form->textField($model,'MaHangSX'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TenSP'); ?>
		<?php echo $form->textField($model,'TenSP',array('size'=>60,'maxlength'=>200)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'GiaSP'); ?>
		<?php echo $form->textField($model,'GiaSP'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TrangThai'); ?>
		<?php echo $form->textField($model,'TrangThai'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'HinhAnh'); ?>
		<?php echo $form->textField($model,'HinhAnh',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'MoTaSP'); ?>
		<?php echo $form->textField($model,'MoTaSP',array('size'=>60,'maxlength'=>10000)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->