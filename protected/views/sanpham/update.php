<?php
/* @var $this SanphamController */
/* @var $model Sanpham */

$this->breadcrumbs=array(
	'Sanphams'=>array('index'),
	$model->MaSP=>array('view','id'=>$model->MaSP),
	'Update',
);

$this->menu=array(
	array('label'=>'List Sanpham', 'url'=>array('index')),
	array('label'=>'Create Sanpham', 'url'=>array('create')),
	array('label'=>'View Sanpham', 'url'=>array('view', 'id'=>$model->MaSP)),
	array('label'=>'Manage Sanpham', 'url'=>array('admin')),
);
?>

<h1>Update Sanpham <?php echo $model->MaSP; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>