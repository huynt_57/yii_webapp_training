<?php
/* @var $this LoaispController */
/* @var $model Loaisp */

$this->breadcrumbs=array(
	'Loaisps'=>array('index'),
	$model->MaLoaiSP,
);

$this->menu=array(
	array('label'=>'List Loaisp', 'url'=>array('index')),
	array('label'=>'Create Loaisp', 'url'=>array('create')),
	array('label'=>'Update Loaisp', 'url'=>array('update', 'id'=>$model->MaLoaiSP)),
	array('label'=>'Delete Loaisp', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->MaLoaiSP),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Loaisp', 'url'=>array('admin')),
);
?>

<h1>View Loaisp #<?php echo $model->MaLoaiSP; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'MaLoaiSP',
		'TenLoaiSP',
		'TrangThaiSP',
	),
)); ?>
