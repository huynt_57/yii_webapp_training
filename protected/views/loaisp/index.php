<?php
/* @var $this LoaispController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Loaisps',
);

$this->menu=array(
	array('label'=>'Create Loaisp', 'url'=>array('create')),
	array('label'=>'Manage Loaisp', 'url'=>array('admin')),
);
?>

<h1>Loaisps</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
