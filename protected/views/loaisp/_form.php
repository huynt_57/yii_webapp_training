<?php
/* @var $this LoaispController */
/* @var $model Loaisp */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'loaisp-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'TenLoaiSP'); ?>
		<?php echo $form->textField($model,'TenLoaiSP',array('size'=>25,'maxlength'=>25)); ?>
		<?php echo $form->error($model,'TenLoaiSP'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'TrangThaiSP'); ?>
		<?php echo $form->textField($model,'TrangThaiSP'); ?>
		<?php echo $form->error($model,'TrangThaiSP'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->