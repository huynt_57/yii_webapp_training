<?php
/* @var $this LoaispController */
/* @var $model Loaisp */

$this->breadcrumbs=array(
	'Loaisps'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Loaisp', 'url'=>array('index')),
	array('label'=>'Manage Loaisp', 'url'=>array('admin')),
);
?>

<h1>Create Loaisp</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>