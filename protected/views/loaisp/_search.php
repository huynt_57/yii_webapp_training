<?php
/* @var $this LoaispController */
/* @var $model Loaisp */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'MaLoaiSP'); ?>
		<?php echo $form->textField($model,'MaLoaiSP'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TenLoaiSP'); ?>
		<?php echo $form->textField($model,'TenLoaiSP',array('size'=>25,'maxlength'=>25)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TrangThaiSP'); ?>
		<?php echo $form->textField($model,'TrangThaiSP'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->