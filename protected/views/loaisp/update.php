<?php
/* @var $this LoaispController */
/* @var $model Loaisp */

$this->breadcrumbs=array(
	'Loaisps'=>array('index'),
	$model->MaLoaiSP=>array('view','id'=>$model->MaLoaiSP),
	'Update',
);

$this->menu=array(
	array('label'=>'List Loaisp', 'url'=>array('index')),
	array('label'=>'Create Loaisp', 'url'=>array('create')),
	array('label'=>'View Loaisp', 'url'=>array('view', 'id'=>$model->MaLoaiSP)),
	array('label'=>'Manage Loaisp', 'url'=>array('admin')),
);
?>

<h1>Update Loaisp <?php echo $model->MaLoaiSP; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>