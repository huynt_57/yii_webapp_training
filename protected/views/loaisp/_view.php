<?php
/* @var $this LoaispController */
/* @var $data Loaisp */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('MaLoaiSP')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->MaLoaiSP), array('view', 'id'=>$data->MaLoaiSP)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TenLoaiSP')); ?>:</b>
	<?php echo CHtml::encode($data->TenLoaiSP); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TrangThaiSP')); ?>:</b>
	<?php echo CHtml::encode($data->TrangThaiSP); ?>
	<br />


</div>