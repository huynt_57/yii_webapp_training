<?php
/* @var $this HangsxController */
/* @var $data Hangsx */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('MaHangSX')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->MaHangSX), array('view', 'id'=>$data->MaHangSX)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TenHangSX')); ?>:</b>
	<?php echo CHtml::encode($data->TenHangSX); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TrangThaiHang')); ?>:</b>
	<?php echo CHtml::encode($data->TrangThaiHang); ?>
	<br />


</div>