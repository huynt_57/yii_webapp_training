<?php
/* @var $this HangsxController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Hangsxes',
);

$this->menu=array(
	array('label'=>'Create Hangsx', 'url'=>array('create')),
	array('label'=>'Manage Hangsx', 'url'=>array('admin')),
);
?>

<h1>Hangsxes</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
