<?php
/* @var $this HangsxController */
/* @var $model Hangsx */

$this->breadcrumbs=array(
	'Hangsxes'=>array('index'),
	$model->MaHangSX=>array('view','id'=>$model->MaHangSX),
	'Update',
);

$this->menu=array(
	array('label'=>'List Hangsx', 'url'=>array('index')),
	array('label'=>'Create Hangsx', 'url'=>array('create')),
	array('label'=>'View Hangsx', 'url'=>array('view', 'id'=>$model->MaHangSX)),
	array('label'=>'Manage Hangsx', 'url'=>array('admin')),
);
?>

<h1>Update Hangsx <?php echo $model->MaHangSX; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>