<?php
/* @var $this HangsxController */
/* @var $model Hangsx */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'MaHangSX'); ?>
		<?php echo $form->textField($model,'MaHangSX'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TenHangSX'); ?>
		<?php echo $form->textField($model,'TenHangSX',array('size'=>25,'maxlength'=>25)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TrangThaiHang'); ?>
		<?php echo $form->textField($model,'TrangThaiHang'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->