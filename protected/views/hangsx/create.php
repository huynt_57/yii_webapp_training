<?php
/* @var $this HangsxController */
/* @var $model Hangsx */

$this->breadcrumbs=array(
	'Hangsxes'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Hangsx', 'url'=>array('index')),
	array('label'=>'Manage Hangsx', 'url'=>array('admin')),
);
?>

<h1>Create Hangsx</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>