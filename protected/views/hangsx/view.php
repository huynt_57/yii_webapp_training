<?php
/* @var $this HangsxController */
/* @var $model Hangsx */

$this->breadcrumbs=array(
	'Hangsxes'=>array('index'),
	$model->MaHangSX,
);

$this->menu=array(
	array('label'=>'List Hangsx', 'url'=>array('index')),
	array('label'=>'Create Hangsx', 'url'=>array('create')),
	array('label'=>'Update Hangsx', 'url'=>array('update', 'id'=>$model->MaHangSX)),
	array('label'=>'Delete Hangsx', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->MaHangSX),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Hangsx', 'url'=>array('admin')),
);
?>

<h1>View Hangsx #<?php echo $model->MaHangSX; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'MaHangSX',
		'TenHangSX',
		'TrangThaiHang',
	),
)); ?>
